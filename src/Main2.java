import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

//        2.
//        Пользователь вводит 5 названий файлов. Сохраните в каждый из файл
//        названия ВСЕХ 5 файлов, используйте try.. catch..finally

public class Main2 {
    public static void Main2(String[] args) throws IOException {
        runMain2();
    }

       public static void runMain2() throws IOException {
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


        //подготовка каталога files2/
        File dir2 = new File("files2/");
        File file2;
        if (dir2.exists()) {
            String[] list = dir2.list();
            for (int i = 0; i < list.length; i++) {
                String fullpath = dir2.getAbsolutePath() + "/" + list[i];
//                  System.out.println(fullpath);
                file2 = new File(fullpath);
                System.out.println(file2.delete());
            }
            System.out.println(dir2.delete());
        }

        dir2.mkdir();


        Scanner scanner2 = new Scanner(System.in);
        String[] arrFileName2 = new String[5];
        StringBuilder temp2 = new StringBuilder(" ");
        for (int i = 0; i < 5; i++) {
            //вводим каждое имя файла только такое которое допустимо. поэтому проверяем
            while (true) {
                System.out.println("Введите имя файла (" + (i + 1) + "):");
                //отчистка строки под новое имя файла
                temp2.delete(0, temp2.length());
                //вводим имя файла
                temp2.insert(0, scanner2.nextLine());
                //проверяем его на доступность

                String fullfilename = dir2.getPath() + "/" + temp2;

                BufferedWriter writer = new BufferedWriter(new FileWriter(fullfilename));
                try {
                    writer.write("test");

                    if (!(arrFileName2[0] == null))
                        for (int ii = 0; ii < i; ii++) {
                            if (arrFileName2[ii].equals(temp2.toString()))
                                throw new IOException("файл с таким именем уже есть");
                        }
                    break;
                } catch (IOException e) {
                    System.out.println("невозможно задать такое имя файла (" + temp2 + ") " + e);
                } finally {
                    writer.close();
                }
            }
            //если всё ок записываем имя в массив
            System.out.println(temp2.toString());
            arrFileName2[i] = temp2.toString();
        }

        for (int i = 0; i < arrFileName2.length; i++) {
            Main.writeToFile(arrFileName2, ("files2/" + arrFileName2[i]));
        }
    }
}
