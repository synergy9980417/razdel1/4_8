import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        //TIP Press <shortcut actionId="ShowIntentionActions"/> with your caret at the highlighted text
        // to see how IntelliJ IDEA suggests fixing it.

//
//        Урок 8. finally
//        1.
//        Пользователь вводит 5 названий файлов. Они могут повторяться. Сохраните в
//        каждый из файл названия ВСЕХ 5 файлов, используйте try with resources

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");


        //подготовка каталога files/
        File dir=new File("files/");
            File file;
          if (dir.exists()) {
              String[] list=dir.list();
              for (int i=0;i<list.length;i++) {
                  String fullpath=dir.getAbsolutePath()+"/"+list[i];
                  file=new File(fullpath);
                  System.out.println(file.delete());
              }
              System.out.println(dir.delete());
          }

          dir.mkdir();


            Scanner scanner1=new Scanner(System.in);
            String[] arrFileName=new String[5];
            StringBuilder temp=new StringBuilder(" ");
            for (int i=0;i<5;i++){
                //вводим каждое имя файла только такое которое допустимо. поэтому проверяем
                while(true) {
                    System.out.println("Введите имя файла (" + (i + 1) + "):");
                    //отчистка строки под новое имя файла
                    temp.delete(0, temp.length());
                    //вводим имя файла
                    temp.insert(0, scanner1.nextLine());
                    //проверяем его на доступность

                    String fullfilename=dir.getPath()+"/"+temp;
                    try (BufferedWriter writer = new BufferedWriter(new FileWriter(fullfilename))) {
                        writer.write("test");
                        if (!(arrFileName[0] ==null))
                        for(int ii=0;ii<i;ii++){
                            if (arrFileName[ii].equals(temp.toString())) throw new IOException("файл с таким именем уже есть");
                        }
                        break;
                    } catch (IOException e) {
                        System.out.println("невозможно задать такое имя файла ("+temp+") "+e);
                    }
                }
                //если всё ок записываем имя в массив
                System.out.println(temp.toString());
              arrFileName[i]=temp.toString();
          }

        for (int i=0;i<arrFileName.length;i++) {
                writeToFile(arrFileName, ("files/"+arrFileName[i]));
        }


  //второе задание
//Реализовано в классе Main2
try {
            Main2.runMain2();
        } catch (IOException e){
            System.out.println(e);
        }
//        Критерии оценивания:
//        1 балл
//                -
//                создан новый проект в IDE
//        2 балла
//                -
//                написана общая структура программы
//        3 балла
//                -
//                выполнено более 60% з
//        аданий, имеется не более 5 критичных замечаний
//        4 балла
//                -
//                выполнено корректно более 80% технических заданий
//        5 баллов
//                -
//                все технические задания выполнены корректно, в полном объеме
//        Задание считается выполненным при условии, что слушатель получил оценку не
//        м
//        енее 3 баллов



//        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
//
//        Scanner scanner = new Scanner(System.in);
//        try {
//            System.out.println("Введите цифру:");
//            char ch = scanner.next().charAt(0);
//            if (!Character.isDigit(ch)) throw new RuntimeException("это не число");
//        } catch (RuntimeException e) {
//            System.err.println("поймали исключение " + e.getMessage());
//            System.out.println(e.getStackTrace());
//            e.printStackTrace();
//        }



    }

    //чистая функция сюда поступает только проверенные предварительно имена файлов, которые точно можно создать
    //запись в файл строку str, возвращает всегда имя файла после операции, которое может поменяться здесь
    public static void writeToFile(String str[], String fileName) {
           try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
            clearFile(fileName);
            for(int i=0;i<str.length;i++) writer.append(str[i]+"\n");
            writer.close();}
           catch (IOException e){
               System.err.println("такого не должно было случиться, сюда поступают только проверенные имена файлов на досупность"+ e);
           }
        }

    //отчистка файла
    public static void clearFile(String file) throws FileNotFoundException {
        PrintWriter writerClear = new PrintWriter(file);
        writerClear.print("");
        writerClear.close();
    }



}